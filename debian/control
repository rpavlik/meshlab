Source: meshlab
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ryan Pavlik <ryan@ryanpavlik.com>,
           Teemu Ikonen <tpikonen@gmail.com>,
           Gürkan Myczko <gurkan@phys.ethz.ch>
Section: graphics
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 12),
               lib3ds-dev,
               libbz2-dev,
               libeigen3-dev,
               libglew-dev,
               libglu1-mesa-dev,
               libgmp-dev,
               libmuparser-dev,
               libopenctm-dev,
               libqhull-dev,
               libqt5opengl5-desktop-dev,
               libqt5xmlpatterns5-dev,
               qtdeclarative5-dev,
               mesa-common-dev,
               qtbase5-dev,
               qtscript5-dev,
               libchealpix-dev,
               libcfitsio-dev,
               zlib1g-dev,
               libpng-dev,
               libjpeg-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/science-team/meshlab
Vcs-Git: https://salsa.debian.org/science-team/meshlab.git
Homepage: https://www.meshlab.net/
Rules-Requires-Root: no

Package: meshlab
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: chemical-mime-data
Description: System for processing and editing triangular meshes
 This is an open source, portable, and extendible system for the
 processing and editing of unstructured 3D triangular meshes.
 The system is aimed to help the processing of the typical not-so-small
 unstructured models arising in 3D scanning, providing a set of tools for
 editing, cleaning, healing, inspecting, rendering and converting this kind
 of meshes.
 .
 Meshlab can read files in these formats: PLY, STL, OFF, OBJ, 3DS, COLLADA
 and PTX. It can write PLY, STL, OFF, OBJ, 3DS, COLLADA, VRML, and DXF.
